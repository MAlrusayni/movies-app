import React, { CSSProperties, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Image, Space, Input, Card, Row, Col, Divider, Typography, Rate, Radio, RadioChangeEvent, BackTop } from 'antd';
import { MovieDb } from 'moviedb-promise'
import { DiscoverMovieResponse, MovieResult, MovieResultsResponse } from 'moviedb-promise/dist/request-types';
import { JsxFlags } from 'typescript';

const API_KEY = "db85dca5d8804d6ffbf1793dc1184d85";

enum QueryMethod {
  Latest,
  Upcoming,
  Popular,
  BestRated,
}

interface AppState {
  movies: Array<MovieResult>,
  moviedb: MovieDb,
  queryMethod: QueryMethod,
}

class App extends React.Component<{}, AppState> {
  state: AppState = {
    movies: Array(),
    moviedb: new MovieDb(API_KEY),
    queryMethod: QueryMethod.Upcoming,
  };

  onSearch: (value: string) => void = (value: string) => {
    if (value.trim() === "") {
      switch (this.state.queryMethod) {
        case QueryMethod.Latest:
          this.state.moviedb.discoverMovie({ sort_by: 'release_date.desc' })
            .then((res) => this.setState({ movies: res.results ?? Array() }))
            .catch((err) => console.log(err));
          break;

        case QueryMethod.Upcoming:
          this.state.moviedb.upcomingMovies({})
            .then((res) => this.setState({ movies: res.results ?? Array() }))
            .catch((err) => console.log(err));
          break;

        case QueryMethod.Popular:
          this.state.moviedb.discoverMovie({ sort_by: 'popularity.desc' })
            .then((res) => this.setState({ movies: res.results ?? Array() }))
            .catch((err) => console.log(err));
          break;

        case QueryMethod.BestRated:
          this.state.moviedb.discoverMovie({ sort_by: 'vote_average.desc' })
            .then((res) => this.setState({ movies: res.results ?? Array() }))
            .catch((err) => console.log(err));
          break;

        default:
          break
      };

    } else {
      this.state.moviedb.searchMovie({ query: value.trim() })
        .then((res) => this.setState({ movies: res.results ?? Array() }))
        .catch((err) => console.log(err));
    }
  }

  constructor(props: {}) {
    super(props);
  }

  componentDidMount() {
    this.onSearch("");
  }

  render() {
    const onQueryMethodChange = (e: RadioChangeEvent) => {
      switch (e.target.value) {
        case "Upcoming":
          this.setState({ queryMethod: QueryMethod.Upcoming }, () => this.onSearch(""))
          break;

        case "Latest":
          this.setState({ queryMethod: QueryMethod.Latest }, () => this.onSearch(""))
          break;

        case "Popular":
          this.setState({ queryMethod: QueryMethod.Popular }, () => this.onSearch(""))
          break;

        case "BestRated":
          this.setState({ queryMethod: QueryMethod.BestRated }, () => this.onSearch(""))
          break;

      }
    }

    const { Paragraph, Text, Title } = Typography;
    const { Search } = Input;

    const movieCards = (movies: Array<MovieResult>): JSX.Element => {
      if (movies.length == 0) {
        return (<Text>No movies yet</Text>);
      } else {
        return (
          <Row wrap gutter={[18, 18]} justify="center">
            {movies.map((movie, index) => {
              return (
                <Col key={index}>
                  <Card
                    size='small'
                    cover={<Image alt="movie-poster" preview={false} src={"https://image.tmdb.org/t/p/w342" + movie.poster_path}
                      fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                    />}
                    style={{ width: 300 }}>
                    <Rate allowHalf count={10} value={movie.vote_average} disabled style={{ position: 'absolute', width: 300, transform: 'translate(-50%, calc(-50% - 30px))' }} />
                    <Title level={5}>{movie.title}</Title>
                    <Paragraph ellipsis={{ rows: 3, expandable: true }}>{movie.overview}</Paragraph>
                    <Text type="secondary">{"publish date: " + movie.release_date}</Text>
                  </Card>
                </Col>
              );
            })
            }
          </Row >
        );
      }
    };

    const bttStyle: React.CSSProperties = {
      height: 40,
      width: 40,
      lineHeight: '40px',
      borderRadius: 4,
      backgroundColor: '#1088e9',
      color: '#fff',
      textAlign: 'center',
      fontSize: 14,
    };

    return (
      <div className="App">
        <Col style={{ margin: 8 }}>
          <Row gutter={12} align="middle" justify="center" style={{ marginBottom: 30, marginTop: 30 }}>
            <Col flex="350px">
              <Search onSearch={this.onSearch} placeholder="Search .." size='large' />
            </Col>
            <Col>
              <Text strong>Sort By: </Text>
              <Radio.Group defaultValue="Upcoming" buttonStyle="solid" size='large' onChange={onQueryMethodChange}>
                <Radio.Button value="Upcoming">Upcoming</Radio.Button>
                <Radio.Button value="Latest">Latest</Radio.Button>
                <Radio.Button value="Popular">Popular</Radio.Button>
                <Radio.Button value="BestRated">Best Rated</Radio.Button>
              </Radio.Group>
            </Col>
          </Row>
          {movieCards(this.state.movies)}
          <BackTop>
            <div style={bttStyle}>UP</div>
          </BackTop>
        </Col>
      </div>
    );
  }
}

export default App;
